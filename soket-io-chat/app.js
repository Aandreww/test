let express = require('express');

const app = express();
let server = require('http').createServer(app);
let io = require('socket.io').listen(server);

let users = {};

server.listen(3000);

app.get('/', (req, res)=> {
    res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', (socket) => {
    socket.on('new user', (data, callback) => {
        if (data in users) {
            callback(false);
        } else {
            callback(true);
            socket.nickname = data;
            users[socket.nickname] = socket;
            updateUserNames();
        }
    });

    socket.on('send message', (data, callback) => {
        let msg = data.trim();
        if (msg.substr(0,3)==='/w ') {
            msg = msg.substr(3);
            let ind = msg.indexOf(' ');
            if (ind !== -1) {
                let name = msg.substring(0, ind);
                msg = msg.substring(ind + 1);
                if (name in users) {
                    users[name].emit('whisper', {msg: msg, nick: socket.nickname});
                    console.log('Message to: ', name);
                } else {
                    callback('Error! Please enter a valid user.');
                }
            } else {
                callback('Error! Please enter a message for your whisper.');
            }
        } else {
            io.sockets.emit('new message', {msg: data, nick: socket.nickname});
        }
    });

    socket.on('disconnect', () => {
        if (!socket.nickname) return;
        delete users[socket.nickname];
        updateUserNames();
    });

    function updateUserNames() {
        io.sockets.emit('nicknames', Object.keys(users));
    }
});
