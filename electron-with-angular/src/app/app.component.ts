import {Component} from '@angular/core';
import { interval } from 'rxjs';
import {takeWhile, tap} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  max = 1;
  current = 0;

  start() {

    interval(100).pipe(takeWhile(() => !this.isFinished),
      tap(() => this.current += 0.1))
      .subscribe();
  }

  finish() {
    this.current = this.max;
  }

  reset() {
     this.current = 0;
  }

  get maxValue() {
    return isNaN(this.max) || this.max < 0.1 ? 0.1 : this.max;
  }

  get currentValue() {
    return isNaN(this.current) || this.current < 0 ? 0 : this.current;
  }

  get isFinished() {
    return this.currentValue >= this.maxValue;
  }
}
